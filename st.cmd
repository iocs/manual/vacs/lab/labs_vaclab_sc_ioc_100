# Startup for LabS-VacLab:SC-IOC-100

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("$(E3_CMD_TOP)/iocsh/labs_vaclab_vac_plc_10000.iocsh", "DBDIR=$(E3_CMD_TOP)/db/, MODVERSION=$(PLCIOCVERSION=$(IOCVERSION=$(DEFAULT_PLCIOCVERSION)))")

